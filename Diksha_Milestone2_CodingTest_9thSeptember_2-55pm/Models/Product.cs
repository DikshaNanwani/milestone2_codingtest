﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Diksha_Milestone2_CodingTest_9thSeptember_2_55pm.Models
{
    public partial class Product
    {
        public Product()
        {
            ProductOrder = new HashSet<ProductOrder>();
        }
        [Required]
        public int ProductId { get; set; }
        [Required]
        public string ProductName { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public decimal? Amount { get; set; }
        [Required]
        public int? Stock { get; set; }
        [Required]
        public int? CatId { get; set; }
        [Required]
        public string Photo { get; set; }

        public virtual Categories Cat { get; set; }
        public virtual ICollection<ProductOrder> ProductOrder { get; set; }
    }
}
