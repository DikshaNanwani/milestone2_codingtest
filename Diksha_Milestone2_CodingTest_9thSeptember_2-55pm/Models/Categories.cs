﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Diksha_Milestone2_CodingTest_9thSeptember_2_55pm.Models
{
    public partial class Categories
    {
        public Categories()
        {
            Product = new HashSet<Product>();
        }

        [Required]
        public int CatId { get; set; }
        [Required]
        public string CategoryName { get; set; }

        public virtual ICollection<Product> Product { get; set; }
    }
}
