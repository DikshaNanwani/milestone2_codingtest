﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Diksha_Milestone2_CodingTest_9thSeptember_2_55pm.Models
{
    public partial class ApplicationUser
    {
        public ApplicationUser()
        {
            ProductOrder = new HashSet<ProductOrder>();
        }
        [Required]
        public int UserId { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public long? MobileNumber { get; set; }
        [Required]
        public int? PinCode { get; set; }
        [Required]
        public string HouseNoBuildingName { get; set; }
        [Required]
        public string RoadArea { get; set; }
        [Required]
        public string City { get; set; }
        [Required]
        public string State { get; set; }

        public virtual ICollection<ProductOrder> ProductOrder { get; set; }
    }
}
