﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Diksha_Milestone2_CodingTest_9thSeptember_2_55pm.Models
{
    public partial class ProductOrder
    {
        [Required]
        public int OrderId { get; set; }
        [Required]
        public int? ProductId { get; set; }
        [Required]
        public int? UserId { get; set; }

        public virtual Product Product { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
}
