﻿using Diksha_Milestone2_CodingTest_9thSeptember_2_55pm.Models;
using System.Collections.Generic;

namespace Diksha_Milestone2_CodingTest_9thSeptember_2_55pm.Business_Layer.Repository
{
    public interface IGroceryServices
    {
        IEnumerable<Product> ViewAllProducts();

        IEnumerable<Categories> ViewAllCategories();

        IEnumerable<ApplicationUser> ViewAllUsers();

        ProductOrder PlaceOrder(int userid, int productid);

        Product GetProductById(int productid);

        IEnumerable<ProductOrder> ShowOrder(int userid);

        IEnumerable<Product> AddProduct(Product product);

    }
}
