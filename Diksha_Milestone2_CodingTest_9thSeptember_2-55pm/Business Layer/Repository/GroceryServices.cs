﻿using Diksha_Milestone2_CodingTest_9thSeptember_2_55pm.Models;
using System.Collections.Generic;
using System.Linq;

namespace Diksha_Milestone2_CodingTest_9thSeptember_2_55pm.Business_Layer.Repository
{
    public class GroceryServices:IGroceryServices
    {
        private readonly GroceriesDbContext _context;

        public GroceryServices(GroceriesDbContext context)
        {
            _context = context;
        }

        public IEnumerable<Product> AddProduct(Product product)
        {
            _context.Product.Add(product);
            _context.SaveChanges();
            return _context.Product.ToList();
        }

        public Product GetProductById(int productid)
        {
            return _context.Product.SingleOrDefault(c => c.ProductId == productid);

        }

        public ProductOrder PlaceOrder(int userid, int productid)
        {
            var order = new ProductOrder()
            {
                UserId = userid,
                ProductId = productid,
                Product = _context.Product.SingleOrDefault(c => c.ProductId == productid),
                User = _context.ApplicationUser.SingleOrDefault(c => c.UserId == userid)
            };
            _context.ProductOrder.Add(order);
            _context.SaveChanges();
            return order;
        }

        public IEnumerable<ProductOrder> ShowOrder(int userid)
        {
            return _context.ProductOrder.Where(p => p.UserId == userid);
        }

        public IEnumerable<Categories> ViewAllCategories()
        {
            return _context.Categories.ToList();
        }

        public IEnumerable<Product> ViewAllProducts()
        {
            return _context.Product.ToList();
        }

        public IEnumerable<ApplicationUser> ViewAllUsers()
        {
            return _context.ApplicationUser.ToList();
        }
    }
}

