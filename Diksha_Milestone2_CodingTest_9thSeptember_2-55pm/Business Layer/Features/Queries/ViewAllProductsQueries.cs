﻿using Diksha_Milestone2_CodingTest_9thSeptember_2_55pm.Models;
using MediatR;
using System.Collections.Generic;

namespace Diksha_CodingTest.Features.Queries
{
    public class ViewAllProductsQueries : IRequest<IEnumerable<Product>>
    {
    }
}
