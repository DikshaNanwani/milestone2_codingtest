﻿using Diksha_Milestone2_CodingTest_9thSeptember_2_55pm.Models;
using MediatR;
using System.Collections.Generic;

namespace Diksha_CodingTest.Features.Queries
{
    public class ShowOrderQueries : IRequest<IEnumerable<ProductOrder>>
    {
        public int UserId { get; set; }
    }
}
