﻿using Diksha_Milestone2_CodingTest_9thSeptember_2_55pm.Models;
using MediatR;

namespace Diksha_CodingTest.Features.Queries
{
    public class GetProductByIdQueries : IRequest<Product>
    {
        public int ProductId { get; set; }
    }
}
