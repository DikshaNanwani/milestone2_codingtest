﻿using Diksha_Milestone2_CodingTest_9thSeptember_2_55pm.Models;
using MediatR;
using System.Collections.Generic;

namespace Diksha_Milestone2_CodingTest_9thSeptember_2_55pm.Business_Layer.Features.Commands
{
    public class AddProductCommand:IRequest<IEnumerable<Product>>
    {
        public Product Product { get; set; }
    }
}
