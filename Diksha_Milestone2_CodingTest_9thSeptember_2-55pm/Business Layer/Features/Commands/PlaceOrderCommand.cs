﻿using Diksha_Milestone2_CodingTest_9thSeptember_2_55pm.Models;
using MediatR;

namespace Diksha_CodingTest.Features.Commands
{
    public class PlaceOrderCommand : IRequest<ProductOrder>
    {
        public int UserId { get; set; }
        public int ProductId { get; set; }
    }
}
