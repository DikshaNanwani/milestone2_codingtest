﻿using Diksha_Milestone2_CodingTest_9thSeptember_2_55pm.Business_Layer.Features.Commands;
using Diksha_Milestone2_CodingTest_9thSeptember_2_55pm.Business_Layer.Repository;
using Diksha_Milestone2_CodingTest_9thSeptember_2_55pm.Models;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Diksha_Milestone2_CodingTest_9thSeptember_2_55pm.Business_Layer.Handlers
{
    public class AddProductHandler : IRequestHandler<AddProductCommand, IEnumerable<Product>>
    {
        private readonly IGroceryServices _data;
        public AddProductHandler(IGroceryServices data)
        {
            _data = data;
        }

        public async Task<IEnumerable<Product>> Handle(AddProductCommand request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_data.AddProduct(request.Product));
        }
    }  
}
