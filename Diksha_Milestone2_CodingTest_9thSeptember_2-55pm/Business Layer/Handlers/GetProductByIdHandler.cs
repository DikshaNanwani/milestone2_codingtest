﻿using Diksha_CodingTest.Features.Queries;
using Diksha_Milestone2_CodingTest_9thSeptember_2_55pm.Business_Layer.Repository;
using Diksha_Milestone2_CodingTest_9thSeptember_2_55pm.Models;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Diksha_CodingTest.Handlers
{
    public class GetProductByIdHandler : IRequestHandler<GetProductByIdQueries, Product>
    {
        private readonly IGroceryServices _data;
        public GetProductByIdHandler(IGroceryServices data)
        {
            _data = data;
        }
        public async Task<Product> Handle(GetProductByIdQueries request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_data.GetProductById(request.ProductId));
        }
    }
}
