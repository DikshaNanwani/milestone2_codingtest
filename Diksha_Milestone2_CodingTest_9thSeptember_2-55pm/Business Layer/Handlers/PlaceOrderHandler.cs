﻿using Diksha_CodingTest.Features.Commands;
using Diksha_Milestone2_CodingTest_9thSeptember_2_55pm.Business_Layer.Repository;
using Diksha_Milestone2_CodingTest_9thSeptember_2_55pm.Models;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Diksha_CodingTest.Handlers
{
    public class PlaceOrderHandler : IRequestHandler<PlaceOrderCommand, ProductOrder>
    {
        private readonly IGroceryServices _data;
        public PlaceOrderHandler(IGroceryServices data)
        {
            _data = data;
        }
        public async Task<ProductOrder> Handle(PlaceOrderCommand request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_data.PlaceOrder(request.UserId, request.ProductId));
        }
    }
}
