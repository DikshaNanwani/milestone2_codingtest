﻿using Diksha_CodingTest.Features.Queries;
using Diksha_Milestone2_CodingTest_9thSeptember_2_55pm.Business_Layer.Repository;
using Diksha_Milestone2_CodingTest_9thSeptember_2_55pm.Models;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Diksha_CodingTest.Handlers
{
    public class ViewAllCategoriesHandler : IRequestHandler<ViewAllCategoriesQueries, IEnumerable<Categories>>
    {
        private readonly IGroceryServices _data;
        public ViewAllCategoriesHandler(IGroceryServices data)
        {
            _data = data;
        }

        public async Task<IEnumerable<Categories>> Handle(ViewAllCategoriesQueries request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_data.ViewAllCategories());
        }
    }
}
