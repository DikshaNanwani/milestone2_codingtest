﻿using Diksha_CodingTest.Features.Queries;
using Diksha_Milestone2_CodingTest_9thSeptember_2_55pm.Business_Layer.Repository;
using Diksha_Milestone2_CodingTest_9thSeptember_2_55pm.Models;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Diksha_CodingTest.Handlers
{
    public class ShowOrderHandler : IRequestHandler<ShowOrderQueries, IEnumerable<ProductOrder>>
    {
        private readonly IGroceryServices _data;
        public ShowOrderHandler(IGroceryServices data)
        {
            _data = data;
        }
        public async Task<IEnumerable<ProductOrder>> Handle(ShowOrderQueries request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_data.ShowOrder(request.UserId));
        }
    }
}
