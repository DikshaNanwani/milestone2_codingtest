﻿using Diksha_CodingTest.Features.Queries;
using Diksha_Milestone2_CodingTest_9thSeptember_2_55pm.Business_Layer.Repository;
using Diksha_Milestone2_CodingTest_9thSeptember_2_55pm.Models;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Diksha_CodingTest.Handlers
{
    public class ViewAllUsersHandler : IRequestHandler<ViewAllUsersQueries, IEnumerable<ApplicationUser>>
    {
        private readonly IGroceryServices _data;
        public ViewAllUsersHandler(IGroceryServices data)
        {
            _data = data;
        }
        public async Task<IEnumerable<ApplicationUser>> Handle(ViewAllUsersQueries request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_data.ViewAllUsers());
        }
    }
}
