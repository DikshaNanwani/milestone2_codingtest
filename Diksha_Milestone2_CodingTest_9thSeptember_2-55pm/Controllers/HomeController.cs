﻿using Diksha_CodingTest.Features.Commands;
using Diksha_CodingTest.Features.Queries;
using Diksha_Milestone2_CodingTest_9thSeptember_2_55pm.Business_Layer.Features.Commands;
using Diksha_Milestone2_CodingTest_9thSeptember_2_55pm.Models;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Diksha_Milestone2_CodingTest_9thSeptember_2_55pm.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        private readonly ISender _sender;

        public HomeController(ISender sender)
        {
            _sender = sender;
        }

        [HttpGet]
        public async Task<IEnumerable<Product>> ViewAllProducts()
        {
            return await _sender.Send(new ViewAllProductsQueries());
        }

        [HttpGet]
        public async Task<IEnumerable<Categories>> ViewAllCategories()
        {
            return await _sender.Send(new ViewAllCategoriesQueries());
        }

        [HttpGet]
        public async Task<IEnumerable<ApplicationUser>> ViewAllUsers()
        {
            return await _sender.Send(new ViewAllUsersQueries());
        }

        [HttpPost]
        public async Task<ProductOrder> PlaceOrder(int userid, int productid)
        {
            return await _sender.Send(new PlaceOrderCommand() { ProductId = productid, UserId = userid });
        }

        [HttpGet]
        public async Task<Product> GetProductById(int productid)
        {
            return await _sender.Send(new GetProductByIdQueries() { ProductId = productid });
        }

        [HttpGet]
        public async Task<IEnumerable<ProductOrder>> ShowOrder(int userid)
        {
            return await _sender.Send(new ShowOrderQueries() { UserId = userid });
        }

        [HttpPost]
        public async Task<IEnumerable<Product>> AddProduct([FromBody] Product product)
        {
            return await _sender.Send(new AddProductCommand() { Product = product });
        }
    }
}





